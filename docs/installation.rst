============
Installation
============

At the command line::

    $ easy_install dj_chart

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj_chart
    $ pip install dj_chart
